import torch
from PIL import Image
from torchvision.transforms import ToPILImage
from torchvision.utils import save_image as sav 
import numpy as np
import matplotlib.pyplot as plt


num_image = 1

class Conv2D:
    """docstring for Conv2D"""
    def __init__(self, in_channel, o_channel, kernel_size, stride, mode):
        self.in_channel = in_channel
        self.o_channel = o_channel
        self.kernel_size = kernel_size
        self.stride = stride
        self.mode = mode
        self.k1 = torch.tensor([[-1.0,-1.0,-1.0],[0,0,0],[1,1,1]]).float()
        self.k2 = torch.tensor([[-1,  0,  1], [-1, 0, 1],[-1, 0, 1]] ).float()
        self.k3 = torch.tensor([[1,  1,  1], [1, 1, 1],  [1, 1, 1]]).float()
        self.k4 = torch.tensor([[-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [0, 0, 0, 0, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]]).float()
        self.k5 = torch.tensor([[-1, -1, 0, 1, 1], [-1, -1, 0, 1, 1], [-1, -1, 0, 1, 1], [-1, -1, 0, 1, 1], [-1, -1, 0, 1, 1]]).float()
        self.krandom = torch.randn(self.kernel_size, self.kernel_size)

    def forward(self, input_image):
    	channel, height, width = input_image.size()
    	out_multiply = torch.zeros(height, width)
    	if self.mode == 'known':
    		if self.o_channel == 1:
	    		#Takes 2D tensor to 3D
		    	tensor_list = [self.k1, self.k1, self.k1 ]
		    	kernel_first = torch.stack(tensor_list)
		    	addition = 0
		    	multiplication = 0

		    	for row in range(height-3):
		    		for coloumn in range(width-3):
		    			#print(input_image[:,row:row+3, coloumn:coloumn+3].size())
		    			result_multiply = torch.mul(self.k1, input_image[:,row:row+3, coloumn:coloumn+3])
		    			out_multiply[row, coloumn] = result_multiply.sum()
		    			addition += 1
		    			multiplication += 1
		    	#Exporting as grayscale image
		    	for i in range(self.o_channel):
		    		global num_image
		    		sav(out_multiply, 'Img%sTask1greyscale_image1.jpg'%(num_image),normalize=True)
		    	#Times the number of element in each kernel
		    	total_iteration = torch.numel(kernel_first) * (addition + multiplication)
		    	return(total_iteration, out_multiply)


	    	elif self.o_channel == 2:
	    		out_multiply = torch.zeros(self.o_channel, height, width)
		    	tensor_list = [self.k4, self.k4, self.k4 ]
		    	kernel_first = torch.stack(tensor_list)
		    	tensor_list = [self.k5, self.k5, self.k5]
		    	kernel_second = torch.stack(tensor_list)
		    	all_kernel = [kernel_first, kernel_second]
		    	addition = 0
		    	multiplication = 0

		    	for channs in range(self.o_channel):
		    		for row in range(height-5):
		    			for coloumn in range(width-5):
		    				result_multiply = torch.mul(all_kernel[channs], input_image[:,row:row+5, coloumn:coloumn+5])
		    				out_multiply[channs,row, coloumn] = result_multiply.sum()
		    				addition += 1
		    				multiplication += 1
		    	#Exporting as grayscale image
		    	for i in range(self.o_channel):
		    		num_image
		    		sav(out_multiply[i,:,:], 'Img%sTask2greyscale_image%s.jpg'%(num_image,i), normalize=True)
		    	
		    	total_iteration = torch.numel(kernel_first) * (addition + multiplication)
		    	return(total_iteration, out_multiply)

	    	elif self.o_channel == 3:
	    		h1 = int(height/self.stride)
	    		w1 = int(width/self.stride)
	    		#Each time move 2 pixle per step thus output will be half the original size
	    		out_multiply = torch.zeros(self.o_channel, h1, w1)
	    		tensor_list = [self.k1, self.k1, self.k1 ]
		    	kernel_first = torch.stack(tensor_list)
		    	tensor_list = [self.k2, self.k2, self.k2 ]
		    	kernel_second = torch.stack(tensor_list)
		    	tensor_list = [self.k3, self.k3, self.k3 ]
		    	kenrel_third = torch.stack(tensor_list)

		    	multiplication = 0
		    	addition = 0

		    	all_kernel = [kernel_first, kernel_second, kenrel_third]

		    	for channs in range(self.o_channel):
		    		for row in range(0,height-self.kernel_size, self.stride):
		    			for coloumn in range(0,width-self.kernel_size, self.stride):
		    				result_multiply = torch.mul(all_kernel[channs], input_image[:,row:row+self.kernel_size, coloumn:coloumn+self.kernel_size])
		    				out_multiply[channs,int(row/self.stride), int(coloumn/self.stride)] = result_multiply.sum()
		    				addition += 1
		    				multiplication += 1 
		    	#Exporting as grayscale image
		    	for i in range(self.o_channel):
		    		num_image
		    		sav(out_multiply[i,:,:], 'Img%sTask3greyscale_image%s.jpg'%(num_image,i), normalize=True)
		    	num_image += 1

		    	total_iteration = torch.numel(kernel_first) * (addition + multiplication)
		    	return(total_iteration, out_multiply)

    	else:
    		out_multiply = torch.zeros(self.o_channel, height, width)
    		#Takes 2D tensor to 3D
    		tensor_list = []
    		for i in range(self.in_channel):
    			tensor_list.append(self.krandom)

    		multiplication = 0
    		addition = 0

    		kernels = torch.stack(tensor_list)
    		for channs in range(self.o_channel):
    			for row in range(height-self.kernel_size):
    				for coloumn in range(width-self.kernel_size):
    					result_multiply = torch.mul(self.krandom, input_image[:,row:row+self.kernel_size, coloumn:coloumn+self.kernel_size])
    					out_multiply[channs,row,coloumn] = result_multiply.sum()
    					multiplication += 1
    					addition += 1
    		total_iteration = torch.numel(kernels) * (addition + multiplication)
    		return(total_iteration, out_multiply)