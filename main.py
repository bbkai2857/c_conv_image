from conv import Conv2D
import torch
from PIL import Image
from torchvision import transforms as transform
from torchvision.transforms import ToTensor
import matplotlib.pyplot as plt
import time 
import numpy as np

#Opens the image
first_input_image = Image.open("1280_720.jpg")
second_input_mage = Image.open("1920_1080.jpg")

all_images = [first_input_image, second_input_mage]

#Part A
for i in range(len(all_images)):
	
	#Convert the image into a tensor
	input_image = ToTensor()(all_images[i])
	#Task 1
	conv2d = Conv2D(in_channel=3, o_channel=1, kernel_size=3, stride=1, mode='known')
	conv2d.forward(input_image)

	#Task 2
	conv2d = Conv2D(in_channel=3,o_channel=2, kernel_size=5,stride=1,mode='known')
	conv2d.forward(input_image)

	#Task 3
	conv2d = Conv2D(in_channel=3, o_channel=3, kernel_size=3,stride=2,mode='known')
	conv2d.forward(input_image)

'''
#Part B
counts = 0
for img in range(len(all_images)):
	for i in range(10):
		elapsed = []
		input_image = ToTensor()(all_images[img])
		conv2d = Conv2D(in_channel=3, o_channel=2**i, kernel_size=3, stride=1, mode='rand')
		start = time.time()
		conv2d.forward(input_image)
		end = time.time()
		elapsed.append(end - start)
	plt.plot(np.arange(0,10,1), np.asarray(elapsed), label = "Line %s" %(counts),linestyle='-', marker='o')
	plt.axis([0, 10, 0, elapsed[len(elapsed) -1]])
	plt.grid(True)
	plt.xlabel('i channel')
	plt.ylabel('Time')
	plt.title('Time V.S i channel')
	plt.savefig('PartBImage.png',bbox_inches = 'tight')
	counts += 1 

'''
'''
#Part C
count = 0
for img in range(len(all_images)):
	total_ops = []
	for i in range(3,11,2):
		input_image = ToTensor()(all_images[img])
		conv2d = Conv2D(in_channel=3, o_channel = 2, kernel_size = i,stride=1,mode='rand')
		number_ops, result = conv2d.forward(input_image)
		total_ops.append(number_ops)
	#Plot operation vs kernel size
	plt.plot(np.arange(3,11,2), np.asarray(total_ops) ,label = "Line%s"%(count), linestyle='-', marker='o')
	plt.axis([2,11,0,total_ops[len(total_ops) - 1]])
	plt.grid(True)
	plt.xlabel('Kernel_size')
	plt.ylabel('Operation')
	plt.title('Operation V.S Kernel_size')
	plt.savefig('PartC.png', bbox_inches = 'tight')
	count += 1
'''